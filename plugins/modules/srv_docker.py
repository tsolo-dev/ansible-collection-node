# Copyright: (c) 2024, Tsolo Storage Systems
# Apache-2.0 Licence

# Ansible task to run a Service using Docker.
# - See srv_podman for running a service with Podman.
# TODO: See srv_docker_compose for running a service with Docker compose.

from __future__ import annotations

from traceback import format_exc

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.common.text.converters import to_native
from ansible_collections.tsolo.node.plugins.module_utils.tsolo_node import (
    Service,
    create_makefile,
    create_systemd,
)

DOCUMENTATION = """
---
module: srv_docker
version_added: "1.0"
short_description: Create opinionated /srv directory for a service on Ubuntu.
description:
  - The M(tsolo.node.srv) module creates services in /srv.
    A systemd service file and Makefile to easily work with an application.
options:
  name:
    description:
      - The name of the service.
    type: str
  container:
    description:
      - Container settings.
    type: dict
    default: {}
    suboptions:
      image:
        description:
          - The container image to use.
        type: str
      label:
        description:
          - The label of the container image to use.
        type: str
        default: "latest"
      privileged:
        description:
          - Give extended privileges to this container. The default is false.
        type: bool
      publish:
        description:
          - Publish a container's port, or range of ports, to the host.
            Format - ip:hostPort:containerPort | ip::containerPort |
            hostPort:containerPort | containerPort
        type: list
        elements: str
      env_file:
        description:
          - Read in a line delimited file of environment variables.
            Doesn't support idempotency. If users changes the file with
            environment variables it's on them to recreate the container.
            The file must be present on the REMOTE machine where actual docker
            is running, not on the controller machine where Ansible is
            executing. If you need to copy the file from controller to remote
            machine, use the copy or slurp module.
        type: path
  makefile:
    description:
      - Add or replace sections in the Makefile created.
      - the makefile section is a dictionary.
      - Each key in the makefile dictionary is the command name in make.
      - Each keys value is a dictionary.
      - The value dictionary can have the following keys: description,
        commands and, depends.
      - Value dictionary description is an optional string.
        Used to display the help message for the command.
      - Value dictionary commands is an optional list of strings.
        The shell commands to execute when this command is called with make.
      - Value dictionary depends is an optional list of strings.
        The commands in make that will be called when this command is called.
    type: dict
    default: {}
attributes:
    check_mode:
        support: none
    diff_mode:
        support: none
    platform:
        platforms: posix
seealso:
- module: ansible.builtin.systemd_service
author:
  - Martin Slabber (martin@tsolo.io)
"""

EXAMPLES = """
- name: Deploy Traefik
  tsolo.node.srv:
    name: traefik
    container:
      image: "{{ traefik_container_image }}"
      label: "{{ traefik_version }}"
      publish:
        - 80:80
        - 443:443
        - 7081:7081
      volumes:
        - /srv/traefik/config:/etc/traefik
"""

RETURN = """
path:
  description: Path to the service directory.
  returned: success
  type: str
  sample: "/srv/myservice"
"""


ARGUMENT_SPEC_CONTAINER = {
    "image": {"type": "str"},
    "label": {"type": "str", "default": "latest"},
    "publish": {"type": "list", "elements": "str"},
    "environment": {"type": "list", "elements": "str"},
    "volumes": {"type": "list", "elements": "str"},
    "hostname": {"type": "str"},
    "privileged": {"type": "bool"},
    "command": {"type": "raw"},
    "cmd_args": {"type": "list", "elements": "str"},
    "device": {"type": "list", "elements": "str"},
    "env_file": {"type": "str"},
    "etc_hosts": {"type": "dict", "aliases": ["add_hosts"]},
}
ARGUMENT_SPEC_SYSTEMD = {
    "load": {"type": "bool", "default": True},
    "prefix": {"type": "str", "default": "srv"},
    "restart_policy": {"type": "str", "default": "on-failure"},
}


def main():
    module = AnsibleModule(
        argument_spec={
            "name": {"required": True, "type": "str"},
            "executable": {"default": "docker", "type": "str"},
            "container": {
                "type": "dict",
                "options": ARGUMENT_SPEC_CONTAINER,
            },
            "makefile": {
                "type": "dict",
            },
            "systemd": {
                "type": "dict",
                "default": {k: v["default"] for k, v in ARGUMENT_SPEC_SYSTEMD.items() if "default" in v},
                "options": ARGUMENT_SPEC_SYSTEMD,
            },
        },
    )

    try:
        service = Service(module, podman=False)
        actions = [
            service.mkdir(),
            create_makefile(service),
            create_systemd(service),
        ]
        module.exit_json(changed=any(actions), path=str(service.path))
    except Exception as error:
        module.fail_json(msg=to_native(error), exception=format_exc())


if __name__ == "__main__":
    main()
