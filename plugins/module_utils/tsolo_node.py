import logging
from pathlib import Path
from typing import Dict, List

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.common.text.converters import to_native

DESCRIPTION = "description"
COMMANDS = "commands"
DEPENDS = "depends"
CMD_LINE_BREAK = "\\\n  "


def get_list(data: Dict, key: str) -> List:
    target = data.get(key, [])
    if target and isinstance(target, (list, tuple)):
        return target
    else:
        return []


class Executable:
    """Holds the container runtime executable.

    Attributes:
        cmd: The path to the command to use. Defaults to 'podman' or 'docker'.
        name: Name of the service.
        path: Directory where service is installed.
        container_spec: The value of container section in the task.
        unit_files: If the service is executed with unit files.
        podman: If the executable is Podman, we do not guess.
    """

    def __init__(
        self,
        cmd: str,
        name: str,
        path: Path,
        container_spec: Dict,
        *,
        unit_files: bool,
        podman: bool,
    ):
        self.podman = podman
        self.container_spec = container_spec
        self.name = name
        self.service_name = f"srv-{name}"
        self.service_path = path

        if podman:
            self.bin = self.find_podman(cmd)
        else:
            self.bin = self.find_docker(cmd)
        self.unit_files = unit_files

    def find_podman(self, executable: str) -> Path:
        if not executable or executable == "podman":
            # TODO: Implement smarter guessing. Use which command.
            executable = "/usr/bin/podman"
        ex_path = Path(executable)
        if ex_path.exists():
            return ex_path
        raise ValueError("Could not find the docker executable.")

    def find_docker(self, executable: str) -> Path:
        # TODO: Implement smarter guessing. Use which command.
        if not executable or executable == "docker":
            executable = "/usr/bin/docker"
        ex_path = Path(executable)
        if ex_path.exists():
            return ex_path
        raise ValueError("Could not find the docker executable.")

    def cmd_str_ps(self) -> str:
        """The full command to do a ps with the executable."""
        return f"{self.bin} ps -a -f name={self.service_name}"

    def cmd_str_stop(self) -> str:
        """The full command to do a stop with the executable."""
        return f"{self.bin} stop {self.service_name}"

    def cmd_str_logs(self, follow: bool = True) -> str:
        """The full command to view the logs with the executable."""
        follow_arg = " -f" if follow else ""
        return f"{self.bin} logs{follow_arg} {self.service_name}"

    def cmd_str_stats(self):
        if self.unit_files:
            cmd = (
                [
                    (
                        f"test -f /srv/{self.name}/{self.name}-cid && "
                        f"{self.bin} "
                        f"stats $(shell cat /srv/{self.name}/{self.name}-cid)"
                    )
                ],
            )
        else:
            cmd = [f"{self.bin} stats {self.service_name}"]
        return " ".join(cmd)

    def cmd_str_run(self) -> str:
        cmd = [str(self.bin), "run", f"--name={self.service_name}", "--rm"]
        if self.unit_files:
            cmd.append("--detach")
        if self.podman:
            cmd.append("--replace")
            cmd.append(CMD_LINE_BREAK)
            file_path = self.service_path / f"{self.name}"
            # Write container ID
            cmd.append(f"--cidfile={file_path}-cid")
            # Write process ID
            cmd.append(f"--conmon-pidfile={file_path}-pid")
        cmd.append(CMD_LINE_BREAK)
        cmd.append("--log-driver=journald")
        cmd.append(CMD_LINE_BREAK)
        # Add arguments from the Ansible task
        # Key-Value Arguments
        for arg in ("env_file", "hostname"):
            value = self.container_spec.get(arg)
            if value:
                podman_arg = arg.replace("_", "-")
                cmd.append(f"--{podman_arg}={value}")
                cmd.append(CMD_LINE_BREAK)
        # Flags Arguments
        for arg in ("privileged", "init"):
            if self.container_spec.get(arg):
                cmd.append(f"--{arg}")
        if cmd[-1] != CMD_LINE_BREAK:
            cmd.append(CMD_LINE_BREAK)
        # Publish ports
        for ports in get_list(self.container_spec, "publish"):
            if ":" in ports:
                cmd.append(f"--publish={ports}")
            else:
                cmd.append(f"--publish={ports}:{ports}")
        if cmd[-1] != CMD_LINE_BREAK:
            cmd.append(CMD_LINE_BREAK)
        # Add volumes
        for volume in get_list(self.container_spec, "volumes"):
            if ":" in volume:
                cmd.append(f"--volume={volume}")
        if cmd[-1] != CMD_LINE_BREAK:
            cmd.append(CMD_LINE_BREAK)
        # Add environment
        for value in get_list(self.container_spec, "environment"):
            # Quite a debate if environment should be a dict or a list.
            # At the end its a list to allow for templating on the key.
            # e.g. - "PG_{{ var1 }}={{ var2 }}
            cmd.append(f"--env={value}")
        if cmd[-1] != CMD_LINE_BREAK:
            cmd.append(CMD_LINE_BREAK)
        cmd.append(
            "{}:{}".format(
                self.container_spec["image"],
                self.container_spec.get("label", "latest"),
            )
        )
        container_command = self.container_spec.get("command")
        if container_command:
            cmd.append(container_command)
        # TODO: Add command and command arguments.
        return " ".join(cmd)

    def cmd_str_pull(self) -> str:
        cmd = [str(self.bin), "pull"]
        cmd.append(
            "{}:{}".format(
                self.container_spec["image"],
                self.container_spec.get("label", "latest"),
            )
        )
        return " ".join(cmd)


class Service:
    """Hold the server settings."""

    def __init__(self, module: AnsibleModule, *, podman: bool):
        self.podman = podman
        self.module = module
        self.name = module.params["name"].lower()
        self.params = module.params
        self.path = Path("/srv") / self.name
        self.systemd_name = self._build_systemd_name()
        self.executable = Executable(
            cmd=self.params.get("executable"),
            name=self.name,
            path=self.path,
            container_spec=self.params.get("container", {}),
            unit_files=self.want_unit_files(),
            podman=self.podman,
        )

    def _build_systemd_name(self):
        prefix = self.params.get("systemd", {}).get("prefix", "srv")
        if prefix:
            return f"{prefix}-{self.name}"
        else:
            return self.name

    def want_systemd_reload(self) -> bool:
        """Check if the service wants systemd daemon-reload to be called.

        Returns:
            If daemon-reload of systemd should be called.
        """
        return self.params.get("systemd", {}).get("load", True)

    def want_unit_files(self) -> bool:
        """Check if the service wants unit files.

        Returns:
            If unit files should be used.
        """
        if self.podman:
            return self.params.get("systemd", {}).get("unit_files", False)
        else:
            return False

    def mkdir(self) -> bool:
        """Create the service directory.

        Returns:
            True if the service directory was created.
        """
        before = self.path.is_dir()
        self.path.mkdir(exist_ok=True)
        return before != self.path.is_dir()


def ansible_run_command(module, cmd: List[str]) -> bool:
    """Run a command.

    Args:
        module: The instance of this module.
        cmd: A list of arguments to run as a command.

    Returns:
        The return code of the command.
    """
    args = [to_native(arg, errors="surrogate_or_strict", nonstring="simplerepr") for arg in cmd]
    rc, stdout, stderr = module.run_command(args, encoding=None)
    if rc != 0:
        msg = f"Command '{args}' exited with {rc}: {stdout} {stderr}"
        raise RuntimeError(msg)

    return rc


def write_lines_file(filename: Path, lines: List) -> bool:
    """Write to a file and check if file changed.

    Args:
        filename: The file to write to.
        lines: A list of lines to write to the file.
            A new line character is added to each line, except the last.

    Returns:
        True if the file changed.
    """
    return write_file(filename, "\n".join(lines))


def write_file(filename: Path, text: str) -> bool:
    """Write to a file and check if file changed.

    Args:
        filename: The file to write to.
        text: The contents to write to the file.

    Returns:
        True if the file changed.
    """
    changed = False
    old_text = ""

    if filename.exists():
        old_text = filename.read_text()

    if old_text != text:
        changed = True
        filename.write_text(text)
    logging.warning("File %s changed %s", filename, changed)
    return changed


def create_makefile_contents(service: Service) -> Dict:
    """Create the sections of the Makefile.

    Args:
        name: Name of the service.
        extra_sections: Additional sections to add the the service Makefile.

    Returns:
        True if any change were made.
    """
    contents = {}
    name = service.name
    srv_name = service.systemd_name
    contents["start"] = {
        DESCRIPTION: "Start the service.",
        COMMANDS: [f"systemctl start {srv_name}"],
    }
    contents["stop"] = {
        DESCRIPTION: "Stop the service.",
        COMMANDS: [f"systemctl stop {srv_name}"],
    }
    contents["enable"] = {
        DESCRIPTION: "Enable the service.",
        COMMANDS: [f"systemctl enable {srv_name}"],
    }
    contents["disable"] = {
        DESCRIPTION: "Disable the service.",
        COMMANDS: [f"systemctl disable {srv_name}"],
    }
    contents["status"] = {
        DESCRIPTION: "Status of the service.",
        COMMANDS: [f"systemctl status {srv_name} --no-pager -l"],
    }
    contents["restart"] = {
        DESCRIPTION: "Restart the service.",
        DEPENDS: ["stop", "start"],
    }
    contents["ps"] = {
        DESCRIPTION: "Show the running container",
        COMMANDS: [service.executable.cmd_str_ps()],
    }
    contents["stats"] = {
        DESCRIPTION: "Show the running statistics for the container.",
        COMMANDS: [service.executable.cmd_str_stats()],
    }
    if service.podman:
        contents["logs"] = {
            DESCRIPTION: "View service logs.",
            COMMANDS: [f"journalctl -fu {srv_name}"],
        }
        contents["lsof"] = {
            DESCRIPTION: "List open files of the container.",
            COMMANDS: [(f"test -f /srv/{name}/{name}-pid && lsof -Pnp $(shell cat /srv/{name}/{name}-pid)")],
        }
    else:
        contents["logs"] = {
            DESCRIPTION: "View service logs.",
            COMMANDS: [service.executable.cmd_str_logs()],
        }

    extra_sections = service.params.get("makefile", {})
    if extra_sections:
        for step_name, steps in extra_sections.items():
            contents[step_name] = {}
            if steps.get(COMMANDS):
                contents[step_name][COMMANDS] = steps[COMMANDS]
            if steps.get(DESCRIPTION):
                contents[step_name][DESCRIPTION] = steps[DESCRIPTION]
    return contents


def create_makefile_text(contents: Dict) -> str:
    """Create the Makefile file text from a contents dictionary.

    Args:
        contents: The different commands of the make file.

    Returns:
        The text contents of the Makefile.
    """
    lines = []
    lines.append(".PHONY: docs")
    lines.append(".DEFAULT_GOAL := help")

    lines.append("help:")
    lines.append('\t@echo "Help"')
    lines.append(
        "\t@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort "
        "| awk 'BEGIN {FS = \":.*?## \"}; "
        "{printf \"\\033[36m%-30s\\033[0m %s\\n\", $$1, $$2}'"
    )
    for name, steps in contents.items():
        lines.append("")
        description = steps.get(DESCRIPTION)
        if description:
            description = " ## " + description.strip()
        else:
            description = ""

        if steps.get(DEPENDS):
            depends = " " + " ".join(steps[DEPENDS])
        else:
            depends = ""

        lines.append(f"{name}:{depends}{description}")
        for cmd in steps.get(COMMANDS, []):
            lines.append(f"\t{cmd}")

    return "\n".join(lines)


def create_makefile(service) -> bool:
    """Create the Makefile file.

    Args:
        path: Path to the service in /srv
        params: The parameters passed into the task.

    Returns:
        True if any change were made.
    """
    makefile = service.path / "Makefile"
    makefile_contents = create_makefile_contents(service)
    new_make = create_makefile_text(makefile_contents)
    return write_file(makefile, new_make)


def create_unit_run(service: Service) -> bool:
    """Create the unit.run file referenced from the service file.

    Args:
        path: Path to the service in /srv
        params: The parameters passed into the task.

    Returns:
        True if any change were made.
    """
    lines = []
    executable = service.executable
    lines.append("set -e")
    cmd = f"! {executable.bin} rm -f {service.name} 2> /dev/null"
    lines.append(cmd)
    lines.append(cmd)
    cmd = f"! {executable.bin} rm -f --storage {service.name} 2> /dev/null"
    lines.append(cmd)
    lines.append(cmd)

    cmd = executable.cmd_str_run()
    return write_lines_file(service.path / "unit.run", lines)


def create_unit_stop(service) -> bool:
    """Create the unit.stop file referenced from the service file.

    Args:
        path: Path to the service in /srv
        params: The parameters passed into the task.

    Returns:
        True if any change were made.
    """
    lines = []
    name = service.name
    executable = service.executable
    cmd = f"! {executable.bin} inspect {name} &>/dev/null || {executable.bin} stop {name}"
    # TODO(MS): Consider: f"/usr/bin/podman stop --ignore --cidfile={path}/{name}-cid"

    lines.append(cmd)
    lines.append(cmd)  # The Ceph unit.stop does this twice, so we will too.
    return write_file(service.path / "unit.stop", "\n".join(lines))


def create_units(service: Service) -> bool:
    """Create the the unit files referenced from the service file.

    Args:
        path: Path to the service in /srv
        params: The parameters passed into the task.

    Returns:
        True if any change were made.
    """
    changed = any([create_unit_run(service), create_unit_stop(service)])
    return changed


def create_systemd_service_section_unit(service: Service) -> List[str]:
    systemd = service.params.get("systemd", {})
    restart = systemd.get("restart_policy")
    pid = f"/srv/{service.name}/{service.name}-pid"
    cid = f"/srv/{service.name}/{service.name}-cid"
    yield "\n[Service]"
    yield "EnvironmentFile=-/etc/environment"
    yield f"ExecStart=/bin/bash /srv/{service.name}/unit.run"
    yield f"ExecStop=-/bin/bash -c 'bash /srv/{service.name}/unit.stop'"
    yield f"ExecStopPost=-/bin/bash /srv/{service.name}/unit.stop"
    if restart:
        yield f"Restart={restart}"
    yield "RestartSec=10s"
    yield "TimeoutStartSec=200"
    yield "TimeoutStopSec=120"
    yield "StartLimitInterval=30min"
    yield "StartLimitBurst=5"
    yield f"ExecStartPre=-/bin/rm -f {pid} {cid}"
    yield f"ExecStopPost=-/bin/rm -f {pid} {cid}"
    yield "Type=notify"
    yield "NotifyAccess=all"
    yield f"PIDFile={pid}"
    # yield "Delegate=yes"
    # yield "KillMode=none"


def create_systemd_service_section(service: Service) -> List[str]:
    systemd = service.params.get("systemd", {})
    executable = service.executable
    restart = systemd.get("restart_policy")
    yield "\n[Service]"
    yield "EnvironmentFile=-/etc/environment"

    if service.podman:
        pid = f"/srv/{service.name}/{service.name}-pid"
        cid = f"/srv/{service.name}/{service.name}-cid"
        yield f"ExecStartPre=-/bin/rm -f {pid} {cid}"
    # yield f"ExecStartPre=-{executable.cmd_str_pull()}"
    yield f"ExecStart={executable.cmd_str_run()}"
    yield f"ExecStop=-{executable.cmd_str_stop()}"
    if restart:
        yield f"Restart={restart}"
    yield "RestartSec=10s"


def create_systemd(service: Service) -> bool:
    """Create the systemd service file.

    Args:
        service: The service definition.

    Returns:
        True if any change were made.
    """
    changed = False
    lines = []
    service_filename = f"{service.systemd_name}.service"

    lines.append("# generated by Ansible tsolo.node.srv")
    lines.append("\n[Unit]")
    lines.append(f"Description=The service {service.name} from {service.path}")
    lines.append("After=network-online.target")
    lines.append("Wants=network-online.target")

    if service.want_unit_files():
        lines.extend(create_systemd_service_section_unit(service))
    else:
        lines.extend(create_systemd_service_section(service))

    lines.append("\n[Install]")
    lines.append("WantedBy=default.target")

    changed = write_lines_file(service.path / service_filename, lines)
    if service.want_systemd_reload():
        changed_service = write_lines_file(Path("/etc/systemd/system") / service_filename, lines)
        if changed_service:
            changed = True
            ansible_run_command(service.module, ["systemctl", "daemon-reload"])

    return changed
