# Roles


## tsolo.node/benchmarks/iperf

Install and run Iperf3

### artifacts_dir

Where the benchmark reports are placed. This is a path on the host
that Ansible run on.

#### Default value:

```yaml
artifacts_dir: '{{ inventory_dir }}/artifacts'
```

### data_net_cidr

The CIDR of the data network interface. This is the interface on the host
that should be used for high speed data transfers.

#### Default value:

```yaml
data_net_cidr: ''
```

### iperf_client_command_arguments



#### Default value:

```yaml
iperf_client_command_arguments: -t 10
```

### iperf_command_arguments

Commandline arguments to add when calling the Iperf client

## tsolo.node/chrony



### chrony_allow_subnets

Chrony servers (chrony_server_enabled=True) will answer NTP requests from
clients on the following allowed subnets.

#### Default value:

```yaml
chrony_allow_subnets:
- 0.0.0.0/0
```

### chrony_peers_group

An Ansible group that contains the nodes used for peer synchronisation and
fallback.

#### Default value:

```yaml
chrony_peers_group: ''
```

### chrony_server_enabled

If chrony should be a NTP server.

#### Default value:

```yaml
chrony_server_enabled: false
```

### ntp_pools

This ntp_pools config will use (up to):
- 4 sources from ntp.ubuntu.com which some are ipv6 enabled
- 2 sources from 2.ubuntu.pool.ntp.org which is ipv6 enabled as well
- 1 source from [01].ubuntu.pool.ntp.org each (ipv4 only atm)
This means by default, up to 6 dual-stack and up to 2 additional IPv4-only
sources will be used.
At the same time it retains some protection against one of the entries being
down (compare to just using one of the lines). See (LP: #1754358) for the
discussion.


About using servers from the NTP Pool Project in general see (LP: #104525).
Approved by Ubuntu Technical Board on 2011-02-08.
See http://www.pool.ntp.org/join.html for more information.

#### Default value:

```yaml
ntp_pools:
  0.ubuntu.pool.ntp.org:
    iburst: true
    maxsources: 2
  1.ubuntu.pool.ntp.org:
    iburst: true
    maxsources: 2
  2.ubuntu.pool.ntp.org:
    iburst: true
    maxsources: 2
  ntp.ubuntu.com:
    iburst: true
    maxsources: 4
```

### ntp_servers

NTP servers to use in addition to the NTP pools.
If you have local or prefer other NTP servers add them to this list.

#### Default value:

```yaml
ntp_servers:
- 0.za.pool.ntp.org
- 1.za.pool.ntp.org
- 2.za.pool.ntp.org
- 3.za.pool.ntp.org
```

### timezone

The timezone the host should run as.
To see options for this variable use the command `timedatectl list-timezones`.

#### Default value:

```yaml
timezone: Africa/Johannesburg
```

## tsolo.node/consul

Install and configure Consul

### consul_datacenter_name

The name of the datacenter the system runs in.
In most cases this is the same as a zone.

#### Default value:

```yaml
consul_datacenter_name: dc-1
```

### consul_enabled

If consul should be installed on this node.
There are some systems that we do not want consul installed on, e.g. head nodes, hypervisors, developer nodes.

#### Default value:

```yaml
consul_enabled: true
```

### consul_gossip_token

A secret to encrypt traffic with in the datacenter cluster.
All nodes in the cluster must use the same token.
Create a new token for each datacenter.

#### Default value:

```yaml
consul_gossip_token: p1c6tzMpKFBA5TcHaCzJWMxxU4dTreuxBGhRE/iocA8=
```
This is a secret, the value of production and development is not shown.

### consul_servers

A list of group names, hostnames, and IP addresses to add to consul as
servers. Ideal is to have 3 or 5 nodes only.

#### Default value:

```yaml
consul_servers:
- cms
```

## tsolo.node/container_registry

Container registry. This role replaces docker-registry.

### container_registry_s3_config



#### Default value:

```yaml
container_registry_s3_config: {}
```

### container_registry_tls_certificate



#### Default value:

```yaml
container_registry_tls_certificate: ''
```

### container_registry_tls_key



#### Default value:

```yaml
container_registry_tls_key: ''
```

### container_registry_url_origin



#### Default value:

```yaml
container_registry_url_origin: ''
```

## tsolo.node/container_registry_client



### container_registry_address



#### Default value:

```yaml
container_registry_address: ''
```

## tsolo.node/container_registry_mirror_client



### container_registry_mirror_address



#### Default value:

```yaml
container_registry_mirror_address: ''
```

## tsolo.node/coredns



### coredns_container_image

The full, URL and tag, name of the container image to use for coredns.

#### Default value:

```yaml
coredns_container_image: quay.io/tsolo/coredns:{{ coredns_version }}
```

### coredns_version

Version of the CoreDNS container to use.

#### Default value:

```yaml
coredns_version: '1.10'
```

### forward_dns_nameservers

DNS servers to forward requests to.

#### Default value:

```yaml
forward_dns_nameservers:
- 8.8.8.8
- 9.9.9.9
```

### local_domains

Replace this with the list of domains that the cluster will answer for.
At least your primary domain needs to be in this list.

#### Default value:

```yaml
local_domains:
- cluster
- local
```

## tsolo.node/devsecops/ufw



### firewall_enabled



#### Default value:

```yaml
firewall_enabled: true
```

## tsolo.node/etcd

Install Etcd cluster

### etcd_clients_port

Port used for client connections.

#### Default value:

```yaml
etcd_clients_port: 2379
```

### etcd_cluster_token

Shared token for nodes in the cluster.

#### Default value:

```yaml
etcd_cluster_token: 74747474747747474747
```
This is a secret, the value of production and development is not shown.

### etcd_container_image

Container image name.

#### Default value:

```yaml
etcd_container_image: gcr.io/etcd-development/etcd
```

### etcd_nodes

Nodes or groups that make up the etcd cluster. Use ansible inventory hostnames and group names.

#### Default value:

```yaml
etcd_nodes: []
```

### etcd_peers_port

Port used for peer connections.

#### Default value:

```yaml
etcd_peers_port: 2380
```

### etcd_scheme

Schema.
DEPRECATED: Will be http unless SSL keys are provided.

#### Default value:

```yaml
etcd_scheme: http
```

### etcd_version

Container image version label

#### Default value:

```yaml
etcd_version: v3.5.14
```

## tsolo.node/gitea/act_runner

Install Gitea Action runner

### gitea_act_runner_container_image

Container image name.

#### Default value:

```yaml
gitea_act_runner_container_image: docker.io/gitea/act_runner
```

### gitea_act_runner_registration_token

The token used to register act runners to gitea.

#### Default value:

```yaml
gitea_act_runner_registration_token: ''
```
This is a secret, the value of production and development is not shown.

### gitea_act_runner_version

Container image version label

#### Default value:

```yaml
gitea_act_runner_version: 0.2.10
```

### gitea_instance_url

The address of the Gitea server.

#### Default value:

```yaml
gitea_instance_url: ''
```

## tsolo.node/gitea/server

Install Gitea

### gitea_container_image

Container image name.

#### Default value:

```yaml
gitea_container_image: docker.io/gitea/gitea
```

### gitea_server_config



#### Default value:

```yaml
gitea_server_config: []
```

### gitea_server_ssh_port

Container image name.

#### Default value:

```yaml
gitea_server_ssh_port: 2224
```

### gitea_service_config

A list of config values in gitea to set.
Each config is in the form of
     - section: server
       option: START_SSH_SERVER
       value: "true"
See Gitea app.ini for details.

### gitea_version

Container image version label

#### Default value:

```yaml
gitea_version: 1.22.1
```

## tsolo.node/haproxy

Install HAProxy

### haproxy_stats_port

The port Haproxy will display connection statistics and make metrics
available for Prometheus.

#### Default value:

```yaml
haproxy_stats_port: 9000
```

## tsolo.node/http_proxy_client



### http_proxy_address

IP Address and port of the proxy server e.g. 192.168.1.1:3128

#### Default value:

```yaml
http_proxy_address: ''
```

## tsolo.node/jenkins/agent

Install Jenkins swarm on the Node. A whole node is used as an Agent.

### jenkins_agent_controller_address

The address of the Jenkins Controller as seen from the agent.

#### Default value:

```yaml
jenkins_agent_controller_address: '{{ jenkins_controller_address }}'
```

### jenkins_swarm_password

Once the Jenkins controller is running create a user called swarm.
Use a long secure password and set this variable to the password.
TODO: Make a swarm user on the controller if this password is defined.

#### Default value:

```yaml
jenkins_swarm_password: swarmsecretpassword
```
This is a secret, the value of production and development is not shown.

## tsolo.node/jenkins/controller

Install the Jenkins Controller. The controller runs as a container in Podman.

### jenkins_admin_password



#### Default value:

```yaml
jenkins_admin_password: 234823i478723648236482736328gddgwegdfdwcbe3fA@73cbdbdyd
```
This is a secret, the value of production and development is not shown.

### jenkins_backup_dir

The path on the server that will be used by Jenkins to store backups.
This path is mounted into the container as /var/backups and
are given open permissions.

#### Default value:

```yaml
jenkins_backup_dir: /var/backups/jenkins
```

### jenkins_controller_address

The address of the Jenkins server as accessed from remote systems,
including Ansible.

#### Default value:

```yaml
jenkins_controller_address: ''
```

### jenkins_controller_admin_password



#### Default value:

```yaml
jenkins_controller_admin_password: ''
```
This is a secret, the value of production and development is not shown.

### jenkins_home_dir

The path on the server that will be used by Jenkins to store settings and
results. This path is mounted into the container as /var/jenkins_home and
are given open permissions.

#### Default value:

```yaml
jenkins_home_dir: /srv/jenkins/home
```

### jenkins_plugins

A list of Jenkins plugins to install.

#### Default value:

```yaml
jenkins_plugins:
- configuration-as-code
- blueocean
- swarm
- git-forensics
- warnings-ng
- code-coverage-api
- atlassian-bitbucket-server-integration
- github-branch-source
- thinBackup
```

### jenkins_version

The version of the Jenkins to install.

#### Default value:

```yaml
jenkins_version: 2.414.2
```

## tsolo.node/keepalived

Setup high availability (HA). Participating
nodes share floating IP addresses.


### keepalived_auth_password

Shared password used to authenticate HA Keepalive nodes to one another.

#### Default value:

```yaml
keepalived_auth_password: dhys92maSa
```
This is a secret, the value of production and development is not shown.

### keepalived_cidr

The IP address and netmask in CIDR format to use as the floating address
for high availability (HA).

#### Example

```yaml
keepalived_cidr
```

#### Default value:

```yaml
keepalived_cidr: ''
```

### keepalived_cidr_list

A list of CIDRs to use for high availability (HA).
Keepalived limits this to 20 addresses.
The addresses will be distributed to different nodes as far as possible.

#### Default value:

```yaml
keepalived_cidr_list:
- '{{ keepalived_cidr }}'
```

### keepalived_interface

The name of the interface to use for Keepalived. This is a host specific setting.

#### Default value:

```yaml
keepalived_interface: '{{ ansible_default_ipv4.interface }}'
```

### keepalived_participation_enabled

If a node should participate in the HA cluster.
This variable is only set at group and host vars level.
It will result in the host not being used for HA purposes.
If there are less than 3 eligible nodes the HA deployment will fail.

#### Default value:

```yaml
keepalived_participation_enabled: true
```

### keepalived_servers

A list of group names, hostnames, and IP addresses to add to keepalived as
servers. Ideal is to have 3 or 5 nodes only.

#### Default value:

```yaml
keepalived_servers:
- cms
```

## tsolo.node/network

Configure network interfaces, hostname, and install and setup necessary services.

### dnsdomainname

The DNS domain name. The inventory name and the dnsdomainname together form
the FQDN for a host.

#### Default value:

```yaml
dnsdomainname: ''
```

### interfaces

A list of network interface definitions.
A netplan config file for each interface will be created.
All key values in interface definition are passed to netplan as ethernet
configuration. The typical fields are 'addresses, routes, dhcp4,
routing-policy, nameservers'.
The following keys in the interface definition are processed and not
necessarily passed on to netplan:


- name - The name of the interface. This is a required field. Not directly passed to netplan.
- iface - An alias for name.
- gateway - The default gateway. Best to only have one default gateway per system. Not directly passed to netplan, the gateway is added to the routes list.
- gateway4 - An alias for gateway.
- routes - The list of network routes. Each route has the format {'to': CIDR, 'via': IP-ADDRESS}. This list is parsed and updated before being passed to netplan.
- address - The IP address used for the interface. The value is added to addresses. Not directly passed to netplan.
- addresses - The list of network addresses for the interface. Normally just one address per interface. This list is parsed and updated before being passed to netplan.

#### Example

```yaml
interfaces:
  - name: eth9
    address: 192.168.1.9/24
    dhcp4: false
    gateway: 192.168.1.1
    routed:
      - to: 10.11.12.0/24
        via: 192.168.1.10
```

#### Default value:

```yaml
interfaces: []
```

### nameservers

The list of nameservers to use for the host.
The file /etc/systemd/resolve.conf is updated with this list.
These nameservers take precedence over the nameservers defined in netplan.

#### Default value:

```yaml
nameservers: []
```

## tsolo.node/postgresql

Install PostgreSQL as a service running from Podman.

### postgresql_container_image

The full, URL and tag, name of the container image to use for postgresql.

#### Default value:

```yaml
postgresql_container_image: docker.io/postgres
```

### postgresql_port

The port number PostgreSQL will listen on.

#### Default value:

```yaml
postgresql_port: 5432
```

### postgresql_postgres_password

The password for the PostgreSQL admin user 'postgres'.

#### Default value:

```yaml
postgresql_postgres_password: "{{ undef(hint='Password for postgres user.')"
```
This is a secret, the value of production and development is not shown.

### postgresql_version

The version of the postgresql container to use.

#### Default value:

```yaml
postgresql_version: 16
```

## tsolo.node/rclone_share



### rclone_share_config



#### Default value:

```yaml
rclone_share_config: {}
```

### rclone_share_remote_path



#### Default value:

```yaml
rclone_share_remote_path: /
```

## tsolo.node/ssh



### ssh_password_auth



#### Default value:

```yaml
ssh_password_auth: true
```

## tsolo.node/swap

Disable swap

### enabled_swap

Control if swap is enabled or disabled.
Limitation of this role is when changing from enabled=false to enabled=true
nothing is done. Swap has to be enabled manually on the system.

#### Default value:

```yaml
enabled_swap: true
```

## tsolo.node/traefik

Install Traefik as a service running from Podman.

### traefik_acme



#### Default value:

```yaml
traefik_acme:
  challenge:
    delayBeforeCheck: 62
    disablePropagationCheck: true
    provider: route53
    resolvers:
    - 1.1.1.1:53
    - 8.8.8.8:53
  staging: true
  type: dns
```

### traefik_consul

Set to the consul DC to use. If not set then Consul is not used.

#### Default value:

```yaml
traefik_consul: "{{ undef(hint='Consul datacenter') }}"
```

### traefik_container_image

The full, URL and tag, name of the container image to use for traefik.

#### Default value:

```yaml
traefik_container_image: docker.io/traefik
```

### traefik_environment



#### Default value:

```yaml
traefik_environment: []
```

### traefik_etcd

Set to the etcd DC to use. If not set then Etcd is not used.

#### Default value:

```yaml
traefik_etcd: "{{ undef(hint='Etcd datacenter') }}"
```

### traefik_ports



#### Default value:

```yaml
traefik_ports:
  internal: 7080
  internalsecure: 7443
  traefik: 7081
  web: 80
  websecure: 443
```

### traefik_version

The version of the traefik container to use.

#### Default value:

```yaml
traefik_version: v3.1
```

## tsolo.node/ubuntu_server

A mix of tasks useful for production Ubuntu servers.
It is highly opinionated settings.


### apt_automatic_updates_enabled

This flag can be used to turn on automatic updated by APT.
In general we prefer APT to make no changes to the system unless
instructed to do so. This is a very conservative approach and is
useful for production.

#### Default value:

```yaml
apt_automatic_updates_enabled: false
```

## tsolo.node/user

Create users on a node.

### node_users



#### Default value:

```yaml
- description >
```

## tsolo.node/vault

Install and configure Vault

### vault_pki_ca_crt



#### Default value:

```yaml
vault_pki_ca_crt: ''
```

### vault_pki_ca_key



#### Default value:

```yaml
vault_pki_ca_key: ''
```