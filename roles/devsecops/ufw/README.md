# tsolo.node/devsecops/ufw

## Table of content

- [Default Variables](#default-variables)
  - [firewall_enabled](#firewall_enabled)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### firewall_enabled

#### Default value

```YAML
firewall_enabled: true
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
