# tsolo.node/traefik

Install Traefik as a service running from Podman.

## Table of content

- [Default Variables](#default-variables)
  - [traefik_acme](#traefik_acme)
  - [traefik_consul](#traefik_consul)
  - [traefik_container_image](#traefik_container_image)
  - [traefik_environment](#traefik_environment)
  - [traefik_etcd](#traefik_etcd)
  - [traefik_ports](#traefik_ports)
  - [traefik_version](#traefik_version)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### traefik_acme

#### Default value

```YAML
traefik_acme:
  staging: true
  type: dns
  challenge:
    disablePropagationCheck: true
    delayBeforeCheck: 62
    provider: route53
    resolvers:
      - 1.1.1.1:53
      - 8.8.8.8:53
```

### traefik_consul

Set to the consul DC to use. If not set then Consul is not used.

#### Default value

```YAML
traefik_consul: "{{ undef(hint='Consul datacenter') }}"
```

### traefik_container_image

The full, URL and tag, name of the container image to use for traefik.

#### Default value

```YAML
traefik_container_image: docker.io/traefik
```

### traefik_environment

#### Default value

```YAML
traefik_environment: []
```

### traefik_etcd

Set to the etcd DC to use. If not set then Etcd is not used.

#### Default value

```YAML
traefik_etcd: "{{ undef(hint='Etcd datacenter') }}"
```

### traefik_ports

#### Default value

```YAML
traefik_ports:
  web: 80
  websecure: 443
  internal: 7080
  internalsecure: 7443
  traefik: 7081
```

### traefik_version

The version of the traefik container to use.

#### Default value

```YAML
traefik_version: v3.1
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
