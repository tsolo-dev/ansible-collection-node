# tsolo.node/rclone_share

## Table of content

- [Default Variables](#default-variables)
  - [rclone_share_config](#rclone_share_config)
  - [rclone_share_remote_path](#rclone_share_remote_path)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### rclone_share_config

#### Default value

```YAML
rclone_share_config: {}
```

### rclone_share_remote_path

#### Default value

```YAML
rclone_share_remote_path: /
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
