#!/bin/bash

REGISTRY_URL=http://localhost:5000
echo "# Resgistry $REGISTRY_URL"
for CATEGORY in $(curl -s -X GET ${REGISTRY_URL}/v2/_catalog | jq -r .repositories[] | sort)
do
        for TAG in $(curl -s -X GET ${REGISTRY_URL}/v2/${CATEGORY}/tags/list | jq -r .tags[] | sort)
        do
                echo "${CATEGORY}:${TAG}"
        done
done
