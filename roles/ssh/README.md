# tsolo.node/ssh

## Table of content

- [Default Variables](#default-variables)
  - [ssh_password_auth](#ssh_password_auth)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### ssh_password_auth

#### Default value

```YAML
ssh_password_auth: true
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
