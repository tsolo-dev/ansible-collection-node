#!/bin/bash

SEALED=$(vault status -address http://127.0.0.1:8210 -format=json | jq -r .sealed)

for K in $(cat /etc/vault.d/init.json | jq .unseal_keys_b64[] -r | shuf)
do
	vault operator unseal -address http://127.0.0.1:8210 ${K}
	SEALED=$(vault status -address http://127.0.0.1:8210 -format=json | jq -r .sealed)
	if [ "$SEALED" == "false" ]
	then
		break
	fi
done
