# tsolo.node/vault

Install and configure Vault

## Table of content

- [Default Variables](#default-variables)
  - [vault_pki_ca_crt](#vault_pki_ca_crt)
  - [vault_pki_ca_key](#vault_pki_ca_key)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### vault_pki_ca_crt

#### Default value

```YAML
vault_pki_ca_crt: ''
```

### vault_pki_ca_key

#### Default value

```YAML
vault_pki_ca_key: ''
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
