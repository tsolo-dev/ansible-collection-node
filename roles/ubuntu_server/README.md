# tsolo.node/ubuntu_server

A mix of tasks useful for production Ubuntu servers.
It is highly opinionated settings.


## Table of content

- [Default Variables](#default-variables)
  - [apt_automatic_updates_enabled](#apt_automatic_updates_enabled)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### apt_automatic_updates_enabled

This flag can be used to turn on automatic updated by APT. In general we prefer APT to make no changes to the system unless instructed to do so. This is a very conservative approach and is useful for production.

#### Default value

```YAML
apt_automatic_updates_enabled: false
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
