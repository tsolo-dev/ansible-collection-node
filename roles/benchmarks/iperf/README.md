# tsolo.node/benchmarks/iperf

Install and run Iperf3

## Table of content

- [Default Variables](#default-variables)
  - [artifacts_dir](#artifacts_dir)
  - [data_net_cidr](#data_net_cidr)
  - [iperf_client_command_arguments](#iperf_client_command_arguments)
  - [iperf_command_arguments](#iperf_command_arguments)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### artifacts_dir

Where the benchmark reports are placed. This is a path on the host that Ansible run on.

#### Default value

```YAML
artifacts_dir: '{{ inventory_dir }}/artifacts'
```

### data_net_cidr

The CIDR of the data network interface. This is the interface on the host that should be used for high speed data transfers.

#### Default value

```YAML
data_net_cidr: ''
```

### iperf_client_command_arguments

#### Default value

```YAML
iperf_client_command_arguments: -t 10
```

### iperf_command_arguments

Commandline arguments to add when calling the Iperf client



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
