# tsolo.node/container_registry

Container registry. This role replaces docker-registry.

## Table of content

- [Default Variables](#default-variables)
  - [container_registry_s3_config](#container_registry_s3_config)
  - [container_registry_tls_certificate](#container_registry_tls_certificate)
  - [container_registry_tls_key](#container_registry_tls_key)
  - [container_registry_url_origin](#container_registry_url_origin)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### container_registry_s3_config

#### Default value

```YAML
container_registry_s3_config: {}
```

### container_registry_tls_certificate

#### Default value

```YAML
container_registry_tls_certificate: ''
```

### container_registry_tls_key

#### Default value

```YAML
container_registry_tls_key: ''
```

### container_registry_url_origin

#### Default value

```YAML
container_registry_url_origin: ''
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
