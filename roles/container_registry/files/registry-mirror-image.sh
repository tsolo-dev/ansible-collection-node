#!/bin/bash
set -eu
INPUT=$1
TAG=''
if [[ "$INPUT" =~ ":" ]]
then
        TAG=$(echo "$INPUT" | awk -F: '{print $NF}')
fi
if [ -z "${TAG}" ]
then
        TAG=latest
fi
INFO=$(skopeo inspect "docker://${INPUT}")
NAME=$(echo $INFO | jq -r .Name | sed -e " s/docker.io\/library\//docker.io\//")
IMAGE=$(echo "${NAME}" | cut -d/ -f2-)
SRC="docker://${NAME}:${TAG}"
DST="docker://127.0.0.1:5000/${IMAGE}:${TAG}"
echo "copy $SRC $DST"
skopeo copy "$SRC" "$DST"
