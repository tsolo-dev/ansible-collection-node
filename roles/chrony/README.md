# tsolo.node/chrony

## Table of content

- [Default Variables](#default-variables)
  - [chrony_allow_subnets](#chrony_allow_subnets)
  - [chrony_peers_group](#chrony_peers_group)
  - [chrony_server_enabled](#chrony_server_enabled)
  - [ntp_pools](#ntp_pools)
  - [ntp_servers](#ntp_servers)
  - [timezone](#timezone)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### chrony_allow_subnets

Chrony servers (chrony_server_enabled=True) will answer NTP requests from clients on the following allowed subnets.

#### Default value

```YAML
chrony_allow_subnets:
  - 0.0.0.0/0
```

### chrony_peers_group

An Ansible group that contains the nodes used for peer synchronisation and fallback.

#### Default value

```YAML
chrony_peers_group: ''
```

### chrony_server_enabled

If chrony should be a NTP server.

#### Default value

```YAML
chrony_server_enabled: false
```

### ntp_pools

This ntp_pools config will use (up to): - 4 sources from ntp.ubuntu.com which some are ipv6 enabled - 2 sources from 2.ubuntu.pool.ntp.org which is ipv6 enabled as well - 1 source from [01].ubuntu.pool.ntp.org each (ipv4 only atm) This means by default, up to 6 dual-stack and up to 2 additional IPv4-only sources will be used. At the same time it retains some protection against one of the entries being down (compare to just using one of the lines). See (LP: #1754358) for the discussion.

About using servers from the NTP Pool Project in general see (LP: #104525). Approved by Ubuntu Technical Board on 2011-02-08. See http://www.pool.ntp.org/join.html for more information.

#### Default value

```YAML
ntp_pools:
  0.ubuntu.pool.ntp.org:
    iburst: true
    maxsources: 2
  1.ubuntu.pool.ntp.org:
    iburst: true
    maxsources: 2
  2.ubuntu.pool.ntp.org:
    iburst: true
    maxsources: 2
  ntp.ubuntu.com:
    iburst: true
    maxsources: 4
```

### ntp_servers

NTP servers to use in addition to the NTP pools. If you have local or prefer other NTP servers add them to this list.

#### Default value

```YAML
ntp_servers:
  - 0.za.pool.ntp.org
  - 1.za.pool.ntp.org
  - 2.za.pool.ntp.org
  - 3.za.pool.ntp.org
```

### timezone

The timezone the host should run as. To see options for this variable use the command `timedatectl list-timezones`.

#### Default value

```YAML
timezone: Africa/Johannesburg
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
