# tsolo.node/postgresql

Install PostgreSQL as a service running from Podman.

## Table of content

- [Default Variables](#default-variables)
  - [postgresql_container_image](#postgresql_container_image)
  - [postgresql_port](#postgresql_port)
  - [postgresql_postgres_password](#postgresql_postgres_password)
  - [postgresql_version](#postgresql_version)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### postgresql_container_image

The full, URL and tag, name of the container image to use for postgresql.

#### Default value

```YAML
postgresql_container_image: docker.io/postgres
```

### postgresql_port

The port number PostgreSQL will listen on.

#### Default value

```YAML
postgresql_port: 5432
```

### postgresql_postgres_password

The password for the PostgreSQL admin user 'postgres'.

#### Default value

```YAML
postgresql_postgres_password: "{{ undef(hint='Password for postgres user.')"
```

### postgresql_version

The version of the postgresql container to use.

#### Default value

```YAML
postgresql_version: 16
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
