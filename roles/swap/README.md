# tsolo.node/swap

Disable swap

## Table of content

- [Default Variables](#default-variables)
  - [enabled_swap](#enabled_swap)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### enabled_swap

Control if swap is enabled or disabled. Limitation of this role is when changing from enabled=false to enabled=true nothing is done. Swap has to be enabled manually on the system.

#### Default value

```YAML
enabled_swap: true
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
