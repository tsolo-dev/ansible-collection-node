# tsolo.node/coredns

## Table of content

- [Default Variables](#default-variables)
  - [coredns_container_image](#coredns_container_image)
  - [coredns_version](#coredns_version)
  - [forward_dns_nameservers](#forward_dns_nameservers)
  - [local_domains](#local_domains)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### coredns_container_image

The full, URL and tag, name of the container image to use for coredns.

#### Default value

```YAML
coredns_container_image: quay.io/tsolo/coredns:{{ coredns_version }}
```

### coredns_version

Version of the CoreDNS container to use.

#### Default value

```YAML
coredns_version: '1.10'
```

### forward_dns_nameservers

DNS servers to forward requests to.

#### Default value

```YAML
forward_dns_nameservers:
  - 8.8.8.8
  - 9.9.9.9
```

### local_domains

Replace this with the list of domains that the cluster will answer for. At least your primary domain needs to be in this list.

#### Default value

```YAML
local_domains:
  - cluster
  - local
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
