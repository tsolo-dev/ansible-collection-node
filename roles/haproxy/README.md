# tsolo.node/haproxy

Install HAProxy

## Table of content

- [Default Variables](#default-variables)
  - [haproxy_stats_port](#haproxy_stats_port)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### haproxy_stats_port

The port Haproxy will display connection statistics and make metrics available for Prometheus.

#### Default value

```YAML
haproxy_stats_port: 9000
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
