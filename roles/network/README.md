# tsolo.node/network

Configure network interfaces, hostname, and install and setup necessary services.

## Table of content

- [Default Variables](#default-variables)
  - [dnsdomainname](#dnsdomainname)
  - [interfaces](#interfaces)
  - [nameservers](#nameservers)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### dnsdomainname

The DNS domain name. The inventory name and the dnsdomainname together form the FQDN for a host.

#### Default value

```YAML
dnsdomainname: ''
```

### interfaces

A list of network interface definitions. A netplan config file for each interface will be created. All key values in interface definition are passed to netplan as ethernet configuration. The typical fields are 'addresses, routes, dhcp4, routing-policy, nameservers'. The following keys in the interface definition are processed and not necessarily passed on to netplan:

- name - The name of the interface. This is a required field. Not directly passed to netplan. - iface - An alias for name. - gateway - The default gateway. Best to only have one default gateway per system. Not directly passed to netplan, the gateway is added to the routes list. - gateway4 - An alias for gateway. - routes - The list of network routes. Each route has the format {'to': CIDR, 'via': IP-ADDRESS}. This list is parsed and updated before being passed to netplan. - address - The IP address used for the interface. The value is added to addresses. Not directly passed to netplan. - addresses - The list of network addresses for the interface. Normally just one address per interface. This list is parsed and updated before being passed to netplan.

#### Default value

```YAML
interfaces: []
```

#### Example usage

```YAML
interfaces:
  - name: eth9
    address: 192.168.1.9/24
    dhcp4: false
    gateway: 192.168.1.1
    routed:
      - to: 10.11.12.0/24
        via: 192.168.1.10
```

### nameservers

The list of nameservers to use for the host. The file /etc/systemd/resolve.conf is updated with this list. These nameservers take precedence over the nameservers defined in netplan.

#### Default value

```YAML
nameservers: []
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
