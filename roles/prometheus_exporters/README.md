# tsolo.node/prometheus_exporters

Install Prometheus Node Exporter using APT, setup several file exporters.

## Table of content

- [Discovered Tags](#discovered-tags)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Discovered Tags

**_prometheus-metrics_**


## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
