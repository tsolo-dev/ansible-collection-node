# tsolo.node/gitea/server

Install Gitea

## Table of content

- [Default Variables](#default-variables)
  - [gitea_container_image](#gitea_container_image)
  - [gitea_server_config](#gitea_server_config)
  - [gitea_server_ssh_port](#gitea_server_ssh_port)
  - [gitea_service_config](#gitea_service_config)
  - [gitea_version](#gitea_version)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### gitea_container_image

Container image name.

#### Default value

```YAML
gitea_container_image: docker.io/gitea/gitea
```

### gitea_server_config

#### Default value

```YAML
gitea_server_config: []
```

### gitea_server_ssh_port

Container image name.

#### Default value

```YAML
gitea_server_ssh_port: 2224
```

### gitea_service_config

A list of config values in gitea to set. Each config is in the form of - section: server option: START_SSH_SERVER value: "true" See Gitea app.ini for details.

### gitea_version

Container image version label

#### Default value

```YAML
gitea_version: 1.22.1
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
