# tsolo.node/gitea/act_runner

Install Gitea Action runner

## Table of content

- [Default Variables](#default-variables)
  - [gitea_act_runner_container_image](#gitea_act_runner_container_image)
  - [gitea_act_runner_registration_token](#gitea_act_runner_registration_token)
  - [gitea_act_runner_version](#gitea_act_runner_version)
  - [gitea_instance_url](#gitea_instance_url)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### gitea_act_runner_container_image

Container image name.

#### Default value

```YAML
gitea_act_runner_container_image: docker.io/gitea/act_runner
```

### gitea_act_runner_registration_token

The token used to register act runners to gitea.

#### Default value

```YAML
gitea_act_runner_registration_token: ''
```

### gitea_act_runner_version

Container image version label

#### Default value

```YAML
gitea_act_runner_version: 0.2.10
```

### gitea_instance_url

The address of the Gitea server.

#### Default value

```YAML
gitea_instance_url: ''
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
