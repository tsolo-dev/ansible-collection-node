# tsolo.node/http_proxy_client

## Table of content

- [Default Variables](#default-variables)
  - [http_proxy_address](#http_proxy_address)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### http_proxy_address

IP Address and port of the proxy server e.g. 192.168.1.1:3128

#### Default value

```YAML
http_proxy_address: ''
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
