# tsolo.node/user

Create users on a node.

## Table of content

- [Default Variables](#default-variables)
  - [node_users](#node_users)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### node_users

#### Default value

```YAML
description >
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
