# tsolo.node/cleanup

Perform standard system cleanup tasks.
This is often the last step in the setup of a server.


## Table of content

- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
