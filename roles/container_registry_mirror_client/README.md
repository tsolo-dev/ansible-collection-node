# tsolo.node/container_registry_mirror_client

## Table of content

- [Default Variables](#default-variables)
  - [container_registry_mirror_address](#container_registry_mirror_address)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### container_registry_mirror_address

#### Default value

```YAML
container_registry_mirror_address: ''
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
