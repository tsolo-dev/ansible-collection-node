# tsolo.node/consul

Install and configure Consul

## Table of content

- [Default Variables](#default-variables)
  - [consul_datacenter_name](#consul_datacenter_name)
  - [consul_enabled](#consul_enabled)
  - [consul_gossip_token](#consul_gossip_token)
  - [consul_servers](#consul_servers)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### consul_datacenter_name

The name of the datacenter the system runs in. In most cases this is the same as a zone.

#### Default value

```YAML
consul_datacenter_name: dc-1
```

### consul_enabled

If consul should be installed on this node. There are some systems that we do not want consul installed on, e.g. head nodes, hypervisors, developer nodes.

#### Default value

```YAML
consul_enabled: true
```

### consul_gossip_token

A secret to encrypt traffic with in the datacenter cluster. All nodes in the cluster must use the same token. Create a new token for each datacenter.

#### Default value

```YAML
consul_gossip_token: p1c6tzMpKFBA5TcHaCzJWMxxU4dTreuxBGhRE/iocA8=
```

### consul_servers

A list of group names, hostnames, and IP addresses to add to consul as servers. Ideal is to have 3 or 5 nodes only.

#### Default value

```YAML
consul_servers:
  - cms
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
