# tsolo.node/etcd

Install Etcd cluster

## Table of content

- [Default Variables](#default-variables)
  - [etcd_clients_port](#etcd_clients_port)
  - [etcd_cluster_token](#etcd_cluster_token)
  - [etcd_container_image](#etcd_container_image)
  - [etcd_nodes](#etcd_nodes)
  - [etcd_peers_port](#etcd_peers_port)
  - [etcd_scheme](#etcd_scheme)
  - [etcd_version](#etcd_version)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### etcd_clients_port

Port used for client connections.

#### Default value

```YAML
etcd_clients_port: 2379
```

### etcd_cluster_token

Shared token for nodes in the cluster.

#### Default value

```YAML
etcd_cluster_token: 74747474747747474747
```

### etcd_container_image

Container image name.

#### Default value

```YAML
etcd_container_image: gcr.io/etcd-development/etcd
```

### etcd_nodes

Nodes or groups that make up the etcd cluster. Use ansible inventory hostnames and group names.

#### Default value

```YAML
etcd_nodes: []
```

### etcd_peers_port

Port used for peer connections.

#### Default value

```YAML
etcd_peers_port: 2380
```

### etcd_scheme

Schema. DEPRECATED: Will be http unless SSL keys are provided.

#### Default value

```YAML
etcd_scheme: http
```

### etcd_version

Container image version label

#### Default value

```YAML
etcd_version: v3.5.14
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
