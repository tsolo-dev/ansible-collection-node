# tsolo.node/keepalived

Setup high availability (HA). Participating
nodes share floating IP addresses.


## Table of content

- [Default Variables](#default-variables)
  - [keepalived_auth_password](#keepalived_auth_password)
  - [keepalived_cidr](#keepalived_cidr)
  - [keepalived_cidr_list](#keepalived_cidr_list)
  - [keepalived_interface](#keepalived_interface)
  - [keepalived_participation_enabled](#keepalived_participation_enabled)
  - [keepalived_servers](#keepalived_servers)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### keepalived_auth_password

Shared password used to authenticate HA Keepalive nodes to one another.

#### Default value

```YAML
keepalived_auth_password: dhys92maSa
```

### keepalived_cidr

The IP address and netmask in CIDR format to use as the floating address for high availability (HA).

#### Default value

```YAML
keepalived_cidr: ''
```

#### Example usage

```YAML
keepalived_cidr:
  - 192.168.11.30/24
  - 192.168.11.31/24
```

### keepalived_cidr_list

A list of CIDRs to use for high availability (HA). Keepalived limits this to 20 addresses. The addresses will be distributed to different nodes as far as possible.

#### Default value

```YAML
keepalived_cidr_list:
  - '{{ keepalived_cidr }}'
```

### keepalived_interface

The name of the interface to use for Keepalived. This is a host specific setting.

#### Default value

```YAML
keepalived_interface: '{{ ansible_default_ipv4.interface }}'
```

### keepalived_participation_enabled

If a node should participate in the HA cluster. This variable is only set at group and host vars level. It will result in the host not being used for HA purposes. If there are less than 3 eligible nodes the HA deployment will fail.

#### Default value

```YAML
keepalived_participation_enabled: true
```

### keepalived_servers

A list of group names, hostnames, and IP addresses to add to keepalived as servers. Ideal is to have 3 or 5 nodes only.

#### Default value

```YAML
keepalived_servers:
  - cms
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
