# tsolo.node/jenkins/controller

Install the Jenkins Controller. The controller runs as a container in Podman.

## Table of content

- [Default Variables](#default-variables)
  - [jenkins_admin_password](#jenkins_admin_password)
  - [jenkins_backup_dir](#jenkins_backup_dir)
  - [jenkins_controller_address](#jenkins_controller_address)
  - [jenkins_controller_admin_password](#jenkins_controller_admin_password)
  - [jenkins_home_dir](#jenkins_home_dir)
  - [jenkins_plugins](#jenkins_plugins)
  - [jenkins_version](#jenkins_version)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### jenkins_admin_password

#### Default value

```YAML
jenkins_admin_password: 234823i478723648236482736328gddgwegdfdwcbe3fA@73cbdbdyd
```

### jenkins_backup_dir

The path on the server that will be used by Jenkins to store backups. This path is mounted into the container as /var/backups and are given open permissions.

#### Default value

```YAML
jenkins_backup_dir: /var/backups/jenkins
```

### jenkins_controller_address

The address of the Jenkins server as accessed from remote systems, including Ansible.

#### Default value

```YAML
jenkins_controller_address: ''
```

### jenkins_controller_admin_password

#### Default value

```YAML
jenkins_controller_admin_password: ''
```

### jenkins_home_dir

The path on the server that will be used by Jenkins to store settings and results. This path is mounted into the container as /var/jenkins_home and are given open permissions.

#### Default value

```YAML
jenkins_home_dir: /srv/jenkins/home
```

### jenkins_plugins

A list of Jenkins plugins to install.

#### Default value

```YAML
jenkins_plugins:
  - configuration-as-code
  - blueocean
  - swarm
  - git-forensics
  - warnings-ng
  - code-coverage-api
  - atlassian-bitbucket-server-integration
  - github-branch-source
  - thinBackup
```

### jenkins_version

The version of the Jenkins to install.

#### Default value

```YAML
jenkins_version: 2.414.2
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
