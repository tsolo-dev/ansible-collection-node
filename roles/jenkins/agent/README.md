# tsolo.node/jenkins/agent

Install Jenkins swarm on the Node. A whole node is used as an Agent.

## Table of content

- [Default Variables](#default-variables)
  - [jenkins_agent_controller_address](#jenkins_agent_controller_address)
  - [jenkins_swarm_password](#jenkins_swarm_password)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### jenkins_agent_controller_address

The address of the Jenkins Controller as seen from the agent.

#### Default value

```YAML
jenkins_agent_controller_address: '{{ jenkins_controller_address }}'
```

### jenkins_swarm_password

Once the Jenkins controller is running create a user called swarm. Use a long secure password and set this variable to the password. TODO: Make a swarm user on the controller if this password is defined.

#### Default value

```YAML
jenkins_swarm_password: swarmsecretpassword
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
