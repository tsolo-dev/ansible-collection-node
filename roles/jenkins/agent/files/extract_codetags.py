#!/usr/bin/env python3

import json
from pathlib import Path

# Format lines according to
# https://github.com/jenkinsci/warnings-ng-plugin/blob/master/plugin/src/test/resources/io/jenkins/plugins/analysis/warnings/steps/json-issues.log

# Only report on TODO, FIXME, and BUG from https://peps.python.org/pep-0350/
MATCHES = list(
    zip(
        ("LOW", "NORMAL", "HIGH"),
        [f"# {m}" for m in ["TODO", "FIXME", "BUG"]],
        strict=False,
    )
)

tags = []
tag = {"description": [], "severity": "NORMAL"}
for pattern in ["**/*.py", "**/*.yaml", "**/*.yml"]:
    for file in Path(".").glob(pattern):
        for line_no, line in enumerate(file.read_text().splitlines()):
            for level, tag_match in MATCHES:
                if tag_match in line:
                    tag = {"description": [], "severity": level}
                    tags.append(tag)
                    tag["fileName"] = str(file)
                    tag["lineStart"] = line_no + 1
                    _, desc = line.split("#", 1)
                    tag["description"].append(desc.strip())
                # elif line.startswith('#'):
                #    tag['description'].append(line)
                else:
                    tag = {"description": []}

for tag in tags:
    description = " ".join(tag["description"])
    tag["description"] = description
    tag["message"] = description
    print(json.dumps(tag))  # noqa: T201
