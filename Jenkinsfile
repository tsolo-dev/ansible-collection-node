pipeline {
  agent any
  environment {
    ANSIBLE_COLLECTIONS_PATHS = "/tmp/${env.WORKSPACE}/install/collections"
    ANSIBLE_ROLES_PATHS = "/tmp/${env.WORKSPACE}/install/roles"
  }
  stages {
    stage ("Install") {
      steps {
        sh "mkdir -p reports"
        sh "mkdir -p ${ANSIBLE_COLLECTIONS_PATHS} ${ANSIBLE_ROLES_PATHS}"
        sh "hatch run lint:ansible-galaxy collection install -vvv --force ."
      }
    }
    stage ("Ansible Lint") {
      steps {
        sh "hatch run lint:ansible-galaxy collection list"
        sh "hatch run lint:ansible-lint -p --offline | tee reports/ansible-lint.txt"
        recordIssues(tools: [ansibleLint(pattern: 'reports/ansible-lint.txt', skipSymbolicLinks: true)])
      }
    }
    stage ("YAML Lint") {
      steps {
        sh "hatch run lint:yaml | tee reports/yaml-lint.txt"
        recordIssues(tools: [yamlLint(pattern: 'reports/yaml-lint.txt', skipSymbolicLinks: true)])
      }
    }
    stage ("TODOs") {
      steps {
        sh "/usr/local/bin/extract_codetags.py > reports/todo.txt"
        recordIssues(tools: [issues(pattern: 'reports/todo.txt', skipSymbolicLinks: true, name: "Codetags")])
      }
    }
    stage ("Build Docs") {
      steps {
        sh "hatch run docs:build"
        publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'docs/site', reportFiles: 'index.html', reportName: 'Docs tsolo.node', reportTitles: 'Docs tsolo.node', useWrapperFileDirectly: true])
      }
    }
  }
}
